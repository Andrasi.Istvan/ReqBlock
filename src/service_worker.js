chrome.storage.onChanged.addListener(async (changes, area) => {
    const data = changes.config?.newValue
    if (!data) return;
    console.log(`updating request blocking rules from '${area}' storage`);
    const oldRules = await chrome.declarativeNetRequest.getDynamicRules();
    console.log(`old rules`);
    console.log(oldRules);
    await chrome.declarativeNetRequest.updateDynamicRules({
        removeRuleIds: oldRules.map(rule => rule.id),
        addRules: data
            .split("\n")
            .filter(line => !/^\s*$/.test(line)) // remove empty
            .filter(line => !/^\s*#/.test(line)) // remove comment
            .map((line, i) => ({
                id: i + 1,
                priority: 1,
                action: { type: "block" },
                condition: { urlFilter: line }
            }))
    });
    console.log(`new rules`);
    await chrome.declarativeNetRequest.getDynamicRules().then(console.log);
});
